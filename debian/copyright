Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tiny-dnn
Upstream-Contact: https://github.com/tiny-dnn/tiny-dnn/issues
Source: https://github.com/tiny-dnn/tiny-dnn
Files-Excluded:
 cereal
 data
 third_party/gemmlowp
 vc

Files: *
Copyright: 2013-2016, Taiga Nomi and contributors
License: BSD-3-Clause

Files: cmake/DownloadProject/*
Copyright: 2015, Crascit
License: Expat

Files: cmake/Modules/FindTBB.cmake
Copyright: 2011, Hannes Hofmann
License: Expat

Files: docs/logo/raw/GidoleFont/*
Copyright: 2015, Andreas Larsen <mail@andreaslarsen.dk>
License: OFL-1.1
 This Font Software is licensed under the SIL Open Font License, Version 1.1.

Files: test/testimage/pngsuite/*
Copyright: 1996, 2011, Willem van Schaik
License: PngSuite
 Permission to use, copy, modify and distribute these images for any
 purpose and without fee is hereby granted.

Files: third_party/cpplint.py
Copyright: 2009, Google Inc.
License: BSD-3-Clause

Files: third_party/CLCudaAPI/*
Copyright: 2015, SURFsara
License: Apache-2.0

Files: third_party/stb/*
Copyright: 2014-2016, Sean Barrett <sean@nothings.org>
License: STB
 This software is dual-licensed to the public domain and under the following
 license: you are granted a perpetual, irrevocable license to copy, modify,
 publish, and distribute this file as you see fit.

Files: tiny_dnn/core/kernels/tiny_quantized_matmul_kernel.h
Copyright: 2015, The TensorFlow Authors
License: Apache-2.0

Files: debian/*
Copyright: 2022-2024, Andrius Merkys <merkys@debian.org>
License: BSD-3-Clause

License: Apache-2.0
 On Debian systems, the complete text of the Apache License version 2
 can be found in '/usr/share/common-licenses/Apache-2.0'.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 * Neither the name of tiny-dnn nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
